package com.tritongames.assesment.mj.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tritongames.assesment.mj.BR;
import com.tritongames.assesment.mj.R;
import com.tritongames.assesment.mj.model.Album;
import com.tritongames.assesment.mj.viewmodel.iTunesViewModel;

import java.util.List;


/**
 * Created by yousuf on 1/20/18.
 */

public class ITunesListAdapter extends RecyclerView.Adapter<ItunesViewHolder> {

    private List<Album> albumList;
    private iTunesViewModel viewModel;

    public ITunesListAdapter(List<Album> albumList, iTunesViewModel vm) {
        this.albumList = albumList;
        viewModel= vm;
    }

    public void setAlbumList(List<Album> data) {
        albumList = data;
    }

    @Override
    public ItunesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return ItunesViewHolder.create(parent, R.layout.mj_pictures);
    }

    @Override
    public void onBindViewHolder(ItunesViewHolder holder, int position) {
        holder.binding.setVariable(BR.album, albumList.get(position));
        holder.binding.setVariable(BR.actionCallback, viewModel);
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

}
