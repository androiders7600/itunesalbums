package com.tritongames.assesment.mj.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

public class ItunesViewHolder<T extends ViewDataBinding> extends RecyclerView.ViewHolder {
    public final T binding;

    public ItunesViewHolder(T binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    /**
     * Create view for View holder.
     * @param parent
     * @param layoutId
     * @param <T>
     * @return
     */
    public static <T extends ViewDataBinding> ItunesViewHolder<T> create(
            ViewGroup parent, @LayoutRes int layoutId){
        T binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                layoutId, parent, false);
        return new ItunesViewHolder<>(binding);
    }
}
