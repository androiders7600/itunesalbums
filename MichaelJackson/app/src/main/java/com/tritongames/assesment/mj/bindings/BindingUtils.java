package com.tritongames.assesment.mj.bindings;

import android.databinding.BindingAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yousuf.image.downloader.ImageDownloadManager;

/**
 * Created by yousuf on 1/20/18.
 */

public class BindingUtils {

    @BindingAdapter("loadUrl")
    public static void setImageFromUrl(ImageView view, String url) {
        ImageDownloadManager.getInstance().loadBitmap(view, url, true);
    }

    @BindingAdapter("text")
    public static void setText(TextView view, Object value) {
        view.setText(String.valueOf(value));
    }
}
