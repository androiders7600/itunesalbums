package com.tritongames.assesment.mj;

import android.app.Application;

import com.tritongames.assesment.mj.service.ITunesServiceAdapter;

/**
 * Created by yousuf on 1/20/18.
 */

public class iTunesApplication extends Application {

    private static iTunesApplication sInstance;
    private ITunesServiceAdapter mAdapter;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        mAdapter = new ITunesServiceAdapter();
    }

    public static iTunesApplication getInstance(){
        return sInstance;
    }

    public ITunesServiceAdapter getNetworkAdapter(){
        return mAdapter;
    }
}
