package com.tritongames.assesment.mj.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by yousuf on 1/20/18.
 */
public class ItunesData {

    @SerializedName("resultCount")
    private int albumCount;

    @SerializedName("results")
    private List<Album> albums;

    public int getAlbumCount() {
        return albumCount;
    }

    public List<Album> getAlbumList() {
        return albums;
    }
}
