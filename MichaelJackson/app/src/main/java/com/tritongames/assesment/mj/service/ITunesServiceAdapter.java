package com.tritongames.assesment.mj.service;

import com.tritongames.assesment.mj.BuildConfig;
import com.tritongames.assesment.mj.model.ItunesData;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by yousuf on 1/20/18.
 */

public class ITunesServiceAdapter {
    ItunesService mApi;

    public ITunesServiceAdapter(){
        Retrofit retrofitRequest = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mApi = retrofitRequest.create(ItunesService.class);
    }

    public Call<ItunesData> getAlbumList(String searchTerm){
        return mApi.getAlbumList(searchTerm);
    }
}
