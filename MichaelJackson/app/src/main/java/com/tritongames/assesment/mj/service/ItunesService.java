package com.tritongames.assesment.mj.service;

import com.tritongames.assesment.mj.model.ItunesData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ItunesService {

    @GET("/search")
    Call<ItunesData> getAlbumList(@Query("term") String searchTerm);
}
