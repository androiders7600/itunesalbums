package com.tritongames.assesment.mj.utils;

import android.content.Context;
import android.content.Intent;

import com.tritongames.assesment.mj.model.Album;
import com.tritongames.assesment.mj.view.ItunesAlbumDetails;

/**
 * Created by yousuf on 1/20/18.
 */

public class IntentHelper {

    public static final String ITUNES_ALBUM = "itunes_album";

    public static void launchItunesDetails(Context ctx, Album album){
        Intent intent = new Intent(ctx, ItunesAlbumDetails.class);
        intent.putExtra(ITUNES_ALBUM, album);
        ctx.startActivity(intent);
    }
}
