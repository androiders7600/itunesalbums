package com.tritongames.assesment.mj.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.tritongames.assesment.mj.R;


public class ItunesAlbumDetails extends Activity {

    private Button done_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mjdetails);

        done_btn = findViewById(R.id.done_btn);

        done_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    public void onDoneClicked(View view){
        finish();
        overridePendingTransition(0,0);
    }
}
