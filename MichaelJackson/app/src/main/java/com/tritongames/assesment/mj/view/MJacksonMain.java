package com.tritongames.assesment.mj.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;

import android.databinding.DataBindingUtil;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.tritongames.assesment.mj.R;
import com.tritongames.assesment.mj.adapter.ITunesListAdapter;
import com.tritongames.assesment.mj.databinding.ActivityMjacksonMainBinding;
import com.tritongames.assesment.mj.model.Album;
import com.tritongames.assesment.mj.model.ItunesData;
import com.tritongames.assesment.mj.viewmodel.iTunesViewModel;

import java.util.ArrayList;
import java.util.List;

public class MJacksonMain extends AppCompatActivity {

    private ActivityMjacksonMainBinding itunesBinding;
    private iTunesViewModel iTunesVM;

    Observer<ItunesData> itunesData = new Observer<ItunesData>() {
        @Override
        public void onChanged(@Nullable ItunesData data) {
            if (data != null) {
                setAdapterData(data.getAlbumList());
            } else {
                showErrorMessage();
            }
        }
    };

    private void initRecyclerView() {
        itunesBinding.albumList.setLayoutManager(new LinearLayoutManager(this));
        itunesBinding.albumList.setAdapter(new ITunesListAdapter(new ArrayList<Album>(), iTunesVM));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        itunesBinding = DataBindingUtil.setContentView(this, R.layout.activity_mjackson_main);
        initRecyclerView();
        iTunesVM = ViewModelProviders.of(this).get(iTunesViewModel.class);
        iTunesVM.getData("michael jackson").observe(this, itunesData);
    }

    private void setAdapterData(List<Album> data) {
        ((ITunesListAdapter) itunesBinding.albumList.getAdapter()).setAlbumList(data);
    }

    private void showErrorMessage() {
        Snackbar.make(itunesBinding.getRoot(), "No results found. Please try again later.", Snackbar.LENGTH_LONG).show();
    }
}

