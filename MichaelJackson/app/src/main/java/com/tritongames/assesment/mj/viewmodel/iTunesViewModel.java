package com.tritongames.assesment.mj.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.view.View;

import com.tritongames.assesment.mj.iTunesApplication;
import com.tritongames.assesment.mj.model.Album;
import com.tritongames.assesment.mj.model.ItunesData;
import com.tritongames.assesment.mj.utils.IntentHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yousuf on 1/20/18.
 */

public class iTunesViewModel extends ViewModel implements Callback<ItunesData>{

    private MutableLiveData<ItunesData> iTunesLiveData;

    public LiveData<ItunesData> getData(String term){
        if(iTunesLiveData == null){
            iTunesLiveData = new MutableLiveData<>();
        }
        getiTunesData(term);
        return iTunesLiveData;
    }

    private void getiTunesData(String term){
        iTunesApplication.getInstance().getNetworkAdapter().getAlbumList(term).enqueue(this);
    }

    public void onAlbumClicked(View view, Album album){
        IntentHelper.launchItunesDetails(view.getContext(), album);
    }

    @Override
    public void onResponse(retrofit2.Call<ItunesData> call, Response<ItunesData> response) {
        if(response != null) {
            iTunesLiveData.setValue(response.body());
        }
    }

    @Override
    public void onFailure(retrofit2.Call<ItunesData> call, Throwable t) {
        iTunesLiveData.setValue(null);
    }
}
